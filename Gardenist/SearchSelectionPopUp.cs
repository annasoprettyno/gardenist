﻿using System;
using System.Windows.Forms;

namespace Gardenist
{
    public partial class SearchSelectionPopUp : Form
    {
        public SearchSelectionPopUp()
        {
            InitializeComponent();
        }

        private void close_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void name_search_button_Click(object sender, EventArgs e)
        {
            InsertNamePopup inp = new InsertNamePopup();
            inp.ShowDialog();
            inp.TopMost = true;
            this.Close();
        }

        private void order_number_search_button_Click(object sender, EventArgs e)
        {
            NumberInsertPopup nip = new NumberInsertPopup();
            nip.ShowDialog();
            nip.TopMost = true;
            this.Close();
        }
    }
}
