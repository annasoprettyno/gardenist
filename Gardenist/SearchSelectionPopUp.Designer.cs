﻿namespace Gardenist
{
    partial class SearchSelectionPopUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchSelectionPopUp));
            this.close_button = new System.Windows.Forms.Button();
            this.name_search_button = new Gardenist.RoundButton();
            this.order_number_search_button = new Gardenist.RoundButton();
            this.SuspendLayout();
            // 
            // close_button
            // 
            this.close_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(230)))));
            this.close_button.FlatAppearance.BorderSize = 0;
            this.close_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.close_button.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.close_button.Location = new System.Drawing.Point(287, 0);
            this.close_button.Name = "close_button";
            this.close_button.Size = new System.Drawing.Size(35, 33);
            this.close_button.TabIndex = 10;
            this.close_button.Text = "X";
            this.close_button.UseVisualStyleBackColor = false;
            this.close_button.Click += new System.EventHandler(this.close_button_Click);
            // 
            // name_search_button
            // 
            this.name_search_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(138)))), ((int)(((byte)(36)))));
            this.name_search_button.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(138)))), ((int)(((byte)(36)))));
            this.name_search_button.ButtonHighlightColor = System.Drawing.Color.Orange;
            this.name_search_button.ButtonHighlightColor2 = System.Drawing.Color.OrangeRed;
            this.name_search_button.ButtonHighlightForeColor = System.Drawing.Color.Black;
            this.name_search_button.ButtonRoundRadius = 50;
            this.name_search_button.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.name_search_button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.name_search_button.Location = new System.Drawing.Point(35, 86);
            this.name_search_button.Name = "name_search_button";
            this.name_search_button.Size = new System.Drawing.Size(252, 53);
            this.name_search_button.TabIndex = 11;
            this.name_search_button.Text = "ПІБ";
            this.name_search_button.Click += new System.EventHandler(this.name_search_button_Click);
            // 
            // order_number_search_button
            // 
            this.order_number_search_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(138)))), ((int)(((byte)(36)))));
            this.order_number_search_button.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(138)))), ((int)(((byte)(36)))));
            this.order_number_search_button.ButtonHighlightColor = System.Drawing.Color.Orange;
            this.order_number_search_button.ButtonHighlightColor2 = System.Drawing.Color.OrangeRed;
            this.order_number_search_button.ButtonHighlightForeColor = System.Drawing.Color.Black;
            this.order_number_search_button.ButtonRoundRadius = 50;
            this.order_number_search_button.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.order_number_search_button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.order_number_search_button.Location = new System.Drawing.Point(35, 145);
            this.order_number_search_button.Name = "order_number_search_button";
            this.order_number_search_button.Size = new System.Drawing.Size(252, 53);
            this.order_number_search_button.TabIndex = 12;
            this.order_number_search_button.Text = "Номер замовлення";
            this.order_number_search_button.Click += new System.EventHandler(this.order_number_search_button_Click);
            // 
            // SearchSelectionPopUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(322, 240);
            this.Controls.Add(this.order_number_search_button);
            this.Controls.Add(this.name_search_button);
            this.Controls.Add(this.close_button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SearchSelectionPopUp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gardenist";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button close_button;
        private RoundButton name_search_button;
        private RoundButton order_number_search_button;
    }
}