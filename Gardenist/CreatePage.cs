﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gardenist
{
    public partial class CreatePage : Form
    {
        public CreatePage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void back_button_Click(object sender, EventArgs e)
        {
            MainPage mainPage = new MainPage();
            mainPage.Show();
            this.Close();
        }

        private void name_textbox_MouseClick(object sender, MouseEventArgs e)
        {
            name_textbox.Text = "";
            name_textbox.ForeColor = Color.Black;
        }
        private void number_textbox_MouseClick(object sender, MouseEventArgs e)
        {
            phone_textbox.Text = "";
            phone_textbox.ForeColor = Color.Black;
        }
        private void address_textbox_MouseClick(object sender, MouseEventArgs e)
        {
            address_textbox.Text = "";
            address_textbox.ForeColor = Color.Black;
        }
        private void order_textbox_MouseClick(object sender, MouseEventArgs e)
        {
            order_textbox.Text = "";
            order_textbox.ForeColor = Color.Black;
        }

        private void create_button_Click(object sender, EventArgs e)
        {

        }
    }
}
