﻿using System.Windows.Forms;

namespace Gardenist
{
    partial class CreatePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreatePage));
            this.name_textbox = new System.Windows.Forms.TextBox();
            this.phone_textbox = new System.Windows.Forms.TextBox();
            this.address_textbox = new System.Windows.Forms.TextBox();
            this.order_textbox = new System.Windows.Forms.TextBox();
            this.close_button = new System.Windows.Forms.Button();
            this.back_button = new Gardenist.RoundButton();
            this.create_button = new Gardenist.RoundButton();
            this.SuspendLayout();
            // 
            // name_textbox
            // 
            this.name_textbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.name_textbox.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.name_textbox.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.name_textbox.Location = new System.Drawing.Point(155, 134);
            this.name_textbox.Multiline = true;
            this.name_textbox.Name = "name_textbox";
            this.name_textbox.Size = new System.Drawing.Size(240, 24);
            this.name_textbox.TabIndex = 4;
            this.name_textbox.Text = "Тарас Григорович Шевченко";
            this.name_textbox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.name_textbox_MouseClick);
            // 
            // phone_textbox
            // 
            this.phone_textbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.phone_textbox.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.phone_textbox.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.phone_textbox.Location = new System.Drawing.Point(155, 203);
            this.phone_textbox.Multiline = true;
            this.phone_textbox.Name = "phone_textbox";
            this.phone_textbox.Size = new System.Drawing.Size(240, 24);
            this.phone_textbox.TabIndex = 5;
            this.phone_textbox.Text = "380991234455";
            this.phone_textbox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.number_textbox_MouseClick);
            // 
            // address_textbox
            // 
            this.address_textbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.address_textbox.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.address_textbox.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.address_textbox.Location = new System.Drawing.Point(155, 277);
            this.address_textbox.Multiline = true;
            this.address_textbox.Name = "address_textbox";
            this.address_textbox.Size = new System.Drawing.Size(240, 24);
            this.address_textbox.TabIndex = 6;
            this.address_textbox.Text = "Київ, НП 1";
            this.address_textbox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.address_textbox_MouseClick);
            // 
            // order_textbox
            // 
            this.order_textbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.order_textbox.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.order_textbox.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.order_textbox.Location = new System.Drawing.Point(155, 344);
            this.order_textbox.Multiline = true;
            this.order_textbox.Name = "order_textbox";
            this.order_textbox.Size = new System.Drawing.Size(240, 22);
            this.order_textbox.TabIndex = 7;
            this.order_textbox.Text = "Фікус";
            this.order_textbox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.order_textbox_MouseClick);
            // 
            // close_button
            // 
            this.close_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(247)))));
            this.close_button.FlatAppearance.BorderSize = 0;
            this.close_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.close_button.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.close_button.Location = new System.Drawing.Point(514, 1);
            this.close_button.Name = "close_button";
            this.close_button.Size = new System.Drawing.Size(35, 33);
            this.close_button.TabIndex = 8;
            this.close_button.Text = "X";
            this.close_button.UseVisualStyleBackColor = false;
            this.close_button.Click += new System.EventHandler(this.button1_Click);
            // 
            // back_button
            // 
            this.back_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(186)))), ((int)(((byte)(120)))));
            this.back_button.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(186)))), ((int)(((byte)(120)))));
            this.back_button.ButtonHighlightColor = System.Drawing.Color.Orange;
            this.back_button.ButtonHighlightColor2 = System.Drawing.Color.OrangeRed;
            this.back_button.ButtonHighlightForeColor = System.Drawing.Color.Black;
            this.back_button.ButtonRoundRadius = 35;
            this.back_button.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.back_button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.back_button.Location = new System.Drawing.Point(145, 389);
            this.back_button.Name = "back_button";
            this.back_button.Size = new System.Drawing.Size(107, 39);
            this.back_button.TabIndex = 12;
            this.back_button.Text = "Назад";
            this.back_button.Click += new System.EventHandler(this.back_button_Click);
            // 
            // create_button
            // 
            this.create_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(138)))), ((int)(((byte)(36)))));
            this.create_button.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(138)))), ((int)(((byte)(36)))));
            this.create_button.ButtonHighlightColor = System.Drawing.Color.Orange;
            this.create_button.ButtonHighlightColor2 = System.Drawing.Color.OrangeRed;
            this.create_button.ButtonHighlightForeColor = System.Drawing.Color.Black;
            this.create_button.ButtonRoundRadius = 35;
            this.create_button.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.create_button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.create_button.Location = new System.Drawing.Point(281, 389);
            this.create_button.Name = "create_button";
            this.create_button.Size = new System.Drawing.Size(124, 42);
            this.create_button.TabIndex = 13;
            this.create_button.Text = "Створити";
            this.create_button.Click += new System.EventHandler(this.create_button_Click);
            // 
            // CreatePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(550, 492);
            this.Controls.Add(this.create_button);
            this.Controls.Add(this.back_button);
            this.Controls.Add(this.close_button);
            this.Controls.Add(this.order_textbox);
            this.Controls.Add(this.address_textbox);
            this.Controls.Add(this.phone_textbox);
            this.Controls.Add(this.name_textbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CreatePage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gardenist";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox name_textbox;
        private System.Windows.Forms.TextBox phone_textbox;
        private System.Windows.Forms.TextBox address_textbox;
        private System.Windows.Forms.TextBox order_textbox;
        private Button close_button;
        private RoundButton back_button;
        private RoundButton create_button;
    }
}