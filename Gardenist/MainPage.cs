﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Gardenist
{
    public partial class MainPage : Form
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void create_button_Click(object sender, EventArgs e)
        {
            CreatePage createPage = new CreatePage();
            createPage.Show();
            this.Hide();
        }

        private void close_button_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void view_button_Click(object sender, EventArgs e)
        {
            SearchSelectionPopUp selectionPopup = new SearchSelectionPopUp();
            selectionPopup.ShowDialog();
            selectionPopup.TopMost = true;
        }
    }
}
