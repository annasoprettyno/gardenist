﻿using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Gardenist
{
    partial class InsertNamePopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InsertNamePopup));
            this.search_button = new Gardenist.RoundButton();
            this.back_button = new Gardenist.RoundButton();
            this.name_input_textbox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // search_button
            // 
            this.search_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(138)))), ((int)(((byte)(36)))));
            this.search_button.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(138)))), ((int)(((byte)(36)))));
            this.search_button.ButtonHighlightColor = System.Drawing.Color.Orange;
            this.search_button.ButtonHighlightColor2 = System.Drawing.Color.OrangeRed;
            this.search_button.ButtonHighlightForeColor = System.Drawing.Color.Black;
            this.search_button.ButtonRoundRadius = 40;
            this.search_button.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.search_button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.search_button.Location = new System.Drawing.Point(169, 155);
            this.search_button.Name = "search_button";
            this.search_button.Size = new System.Drawing.Size(113, 39);
            this.search_button.TabIndex = 12;
            this.search_button.Text = "Пошук";
            this.search_button.Click += new System.EventHandler(this.create_button_Click);
            // 
            // back_button
            // 
            this.back_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(186)))), ((int)(((byte)(120)))));
            this.back_button.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(186)))), ((int)(((byte)(120)))));
            this.back_button.ButtonHighlightColor = System.Drawing.Color.Orange;
            this.back_button.ButtonHighlightColor2 = System.Drawing.Color.OrangeRed;
            this.back_button.ButtonHighlightForeColor = System.Drawing.Color.Black;
            this.back_button.ButtonRoundRadius = 40;
            this.back_button.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.back_button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.back_button.Location = new System.Drawing.Point(40, 155);
            this.back_button.Name = "back_button";
            this.back_button.Size = new System.Drawing.Size(90, 39);
            this.back_button.TabIndex = 13;
            this.back_button.Text = "Назад";
            this.back_button.Click += new System.EventHandler(this.back_button_Click);
            // 
            // name_input_textbox
            // 
            this.name_input_textbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.name_input_textbox.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.name_input_textbox.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.name_input_textbox.Location = new System.Drawing.Point(40, 98);
            this.name_input_textbox.Multiline = true;
            this.name_input_textbox.Name = "name_input_textbox";
            this.name_input_textbox.Size = new System.Drawing.Size(242, 28);
            this.name_input_textbox.TabIndex = 14;
            this.name_input_textbox.Text = "ПІБ";
            this.name_input_textbox.Click += new System.EventHandler(this.name_textbox_MouseClick);
            // 
            // InsertNamePopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(322, 240);
            this.Controls.Add(this.name_input_textbox);
            this.Controls.Add(this.back_button);
            this.Controls.Add(this.search_button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "InsertNamePopup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InsertNamePopup";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RoundButton search_button;
        private RoundButton back_button;
        private System.Windows.Forms.TextBox name_input_textbox;
    }
}