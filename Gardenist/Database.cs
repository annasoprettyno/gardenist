﻿using System.Data;
using System.Data.SqlClient;


namespace Gardenist
{
    class Database
    {
        static string connectionString = "Data Source=<gardenist-orders.database.windows.net>;Initial Catalog=<GardenistOrders>;User ID=<Anna>;Password=<A237485b679>";
        private SqlConnection connection = new SqlConnection(connectionString);

        public void openConnection()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
                
        }

        public void closeConnection()
        {
            if (connection.State == ConnectionState.Broken)
            {
                connection.Close(); 
            }
        }

        public SqlConnection getConnection()
        {
            return connection;
        }

    }
}
