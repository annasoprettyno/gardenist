﻿using System;
using System.Windows.Forms;

namespace Gardenist
{
    public partial class NumberInsertPopup : Form
    {
        public NumberInsertPopup()
        {
            InitializeComponent();
        }

        private void back_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
