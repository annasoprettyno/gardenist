﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Gardenist
{
    public partial class InsertNamePopup : Form
    {
        public InsertNamePopup()
        {
            InitializeComponent();
        }

        private void back_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void create_button_Click(object sender, EventArgs e)
        {

        }

        private void name_textbox_MouseClick(object sender, EventArgs e)
        {
            name_input_textbox.Text = "";
            name_input_textbox.ForeColor = Color.Black;
        }
    }
}
