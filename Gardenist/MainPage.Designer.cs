﻿using System.Windows.Forms;

namespace Gardenist
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.close_button = new System.Windows.Forms.Button();
            this.create_button = new Gardenist.RoundButton();
            this.view_button = new Gardenist.RoundButton();
            this.SuspendLayout();
            // 
            // close_button
            // 
            this.close_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(247)))));
            this.close_button.FlatAppearance.BorderSize = 0;
            this.close_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.close_button.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.close_button.Location = new System.Drawing.Point(515, 0);
            this.close_button.Name = "close_button";
            this.close_button.Size = new System.Drawing.Size(35, 33);
            this.close_button.TabIndex = 9;
            this.close_button.Text = "X";
            this.close_button.UseVisualStyleBackColor = false;
            this.close_button.Click += new System.EventHandler(this.close_button_Click);
            // 
            // create_button
            // 
            this.create_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(138)))), ((int)(((byte)(36)))));
            this.create_button.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(138)))), ((int)(((byte)(36)))));
            this.create_button.ButtonHighlightColor = System.Drawing.Color.Orange;
            this.create_button.ButtonHighlightColor2 = System.Drawing.Color.OrangeRed;
            this.create_button.ButtonHighlightForeColor = System.Drawing.Color.Black;
            this.create_button.ButtonRoundRadius = 50;
            this.create_button.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.create_button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.create_button.Location = new System.Drawing.Point(45, 160);
            this.create_button.Name = "create_button";
            this.create_button.Size = new System.Drawing.Size(219, 57);
            this.create_button.TabIndex = 10;
            this.create_button.Text = "Створити замовлення";
            this.create_button.Click += new System.EventHandler(this.create_button_Click);
            // 
            // view_button
            // 
            this.view_button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(186)))), ((int)(((byte)(120)))));
            this.view_button.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(186)))), ((int)(((byte)(120)))));
            this.view_button.ButtonHighlightColor = System.Drawing.Color.Orange;
            this.view_button.ButtonHighlightColor2 = System.Drawing.Color.OrangeRed;
            this.view_button.ButtonHighlightForeColor = System.Drawing.Color.Black;
            this.view_button.ButtonRoundRadius = 50;
            this.view_button.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.view_button.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.view_button.Location = new System.Drawing.Point(45, 232);
            this.view_button.Name = "view_button";
            this.view_button.Size = new System.Drawing.Size(219, 57);
            this.view_button.TabIndex = 11;
            this.view_button.Text = "Переглянути замовлення";
            this.view_button.Click += new System.EventHandler(this.view_button_Click);
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(550, 492);
            this.Controls.Add(this.view_button);
            this.Controls.Add(this.create_button);
            this.Controls.Add(this.close_button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gardenist";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion
        private Button close_button;
        private RoundButton create_button;
        private RoundButton view_button;
    }
}

